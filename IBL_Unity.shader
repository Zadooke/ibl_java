﻿Shader "Unlit/IBL_Unlit"
{
	Properties
	{
		_MainTex ("Main Texture", 2D) = "white" {}
		[NoScaleOffset]
		_Normal("Normal", 2D) = "bump" {}
		_Metallic("Metallic", 2D) = "white" {}
		_UseMetallic("Use Metallic", range(0, 1)) = 0
		_Occlusion("Ambient Occlusion", 2D) = "black" {}
		_Cube ("Cube Map", Cube) = "black" {}
		_OcclusionStrenght("Occlusion Strenght", Range(0, 1)) = 0.5
		_Reflectivity("Reflectivity", Range(0,1)) = 0
		_Gloss("Gloss", Range(0,1)) = 0		
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Tags{"LightMode" = "ForwardBase"}
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#define DIFFUSE_MIP_LEVEL 5
			#define GLOSSY_MIP_LEVEL 5			
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex 	: POSITION;
				float3 normal 	: NORMAL;
				float4 tangent 	: TANGENT;
				float2 uv 		: TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex	  	: SV_POSITION;
				float2 uv		  	: TEXCOORD0;
				float3 wNormal	  	: TEXCOORD1;
				float3 wTangent   	: TEXCOORD2;
				float3 wBitangent 	: TEXCOORD3;
				float3 eyeVec		: TEXCOORD4;

			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _Normal;
			sampler2D _Occlusion;
			sampler2D _Metallic;

			float _UseMetallic;

			samplerCUBE _Cube;

			float _OcclusionStrenght;
			float _Reflectivity;
			float _Gloss;
			
			half3 SampleTexCube(samplerCUBE cube, half3 normal, half mip) 
			{
				return texCUBElod(cube, half4(normal, mip));
			}

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				
				o.wNormal = UnityObjectToWorldNormal(v.normal);
				o.wTangent = UnityObjectToWorldNormal(v.tangent.xyz);
				o.wBitangent = cross(o.wNormal, o.wTangent) * v.tangent.w * unity_WorldTransformParams.w;

				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.eyeVec = normalize(worldPos - _WorldSpaceCameraPos);	// Contrario do View Dir

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				half4 albedo = tex2D(_MainTex, i.uv);
				half3 normalTex = tex2D(_Normal, i.uv) * 2 - 1;
				half3 occlusionMap = tex2D(_Occlusion, i.uv);

				half3 finalOcclusion = occlusionMap + (1-_OcclusionStrenght/2); 

				half oneMinusReflectivity = 1 - _Reflectivity;

				if(_UseMetallic >= 1)
				{
					half3 mettalic = tex2D(_Metallic, i.uv);
					oneMinusReflectivity = 1 - mettalic.r;				
				}
				
				half roughness = 1 - _Gloss;	

				half3 N = normalize(i.wTangent * normalTex.r + i.wBitangent * normalTex.g + i.wNormal * normalTex.b);				

				half3 eyeVec = normalize(i.eyeVec);
				half3 R = reflect(eyeVec, N);

				half3 albedoColor = lerp(0, albedo.rgb * (finalOcclusion), oneMinusReflectivity);

				half3 directDiffuse = saturate(dot(N, _WorldSpaceLightPos0));
				half3 indirectDiffuse = SampleTexCube(_Cube, N, DIFFUSE_MIP_LEVEL);

				half3 diffuse = albedoColor * (directDiffuse + indirectDiffuse);

				half3 indirectSpecullar = SampleTexCube(_Cube, R, roughness * GLOSSY_MIP_LEVEL) * _Reflectivity;

				half4 col = 0;
				col.rgb = diffuse + indirectSpecullar;

				return col;
			}
			ENDCG
		}
	}
}
