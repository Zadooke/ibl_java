#version 400

uniform sampler2D uMainTex;
uniform sampler2D uNormal;
uniform sampler2D uOcclusion;
uniform sampler2D uSpecular;

//uniform samplerCube uCube;

uniform float uOcclusionStrenght = 0.5;
uniform float uReflectivity = 0;

//uniform float uGloss = 0;


uniform vec3 uLightDir;

uniform vec3 uAmbientLight;
uniform vec3 uDiffuseLight;
uniform vec3 uSpecularLight;

uniform vec3 uAmbientMaterial;
uniform vec3 uDiffuseMaterial;
uniform vec3 uSpecularMaterial;

uniform float uSpecularPower;

in vec3 vNormal;
in vec3 vViewPath;
in vec2 vTexCoord;

in mat3 vTBN;

// Cor final
out vec4 outColor;

// Mip Map level para pegar a iluminação difusa do cubemap
//#define DIFFUSE_MIP_LEVEL 5
// Mip Map level para pegar o gloss e o indirect diffuse
//#define GLOSSY_MIP_LEVEL 5

void main() {
    // Pega os mapas
    vec4 albedo = vec4(texture(uMainTex, vTexCoord).rgb,1);
    vec4 specularMap = texture(uSpecular, vTexCoord);
    vec4 normalMap = texture(uNormal, vTexCoord);
    vec4 occlusionMap = texture(uOcclusion, vTexCoord);

    vec3 L = normalize(uLightDir);  // Direção da Luz

    vec3 normal = normalize(normalMap.rgb * 2.0 - 1.0);
    vec3 N = normalize(vTBN * normal);    // Direção da Normal

    // Calculo da Luz Ambiente - No futuro, será trocado por um cubemap com mipmap
    vec3 ambient = uAmbientLight * uAmbientMaterial;

    // Calculo do componente difuso
    float diffuseIntensity = max(dot(N, -L), 0.0);
    vec3 diffuse = diffuseIntensity * uDiffuseLight * uDiffuseMaterial;

    // Calculo do componente especular
    float specularIntensity = 0.0;
    if (uSpecularPower > 0.0) {
    	vec3 V = normalize(vViewPath);
    	vec3 R = reflect(L, N);
    	specularIntensity = pow(max(dot(R, V), 0.0), uSpecularPower);
    }

    // Calculo final do Specular
    vec3 specular = specularIntensity * uSpecularLight * uSpecularMaterial * specularMap.r;

    // Calculo do componente de Ambient Occlusion
    float oneMinusReflectivity = 1 - uReflectivity;

    vec3 finalOcclusion = occlusionMap.rgb + (1 - uOcclusionStrenght / 2);
    vec3 albedoColor = mix(vec3(0, 0, 0), albedo.rgb * finalOcclusion, oneMinusReflectivity);

    vec3 color = clamp(albedoColor * (ambient + diffuse) + specular, 0.0, 1.0);
    outColor = vec4(color, albedo.a);
}
