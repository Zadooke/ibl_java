#version 330

in vec3 aPosition;

uniform mat4 uProjection;
uniform mat4 uView;
uniform mat4 uWorld;

void main() {
	gl_Position = uProjection * uView * uWorld * vec4(aPosition, 1.0);
}
