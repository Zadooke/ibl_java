#version 330

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

in vec3 vNormal[3];

const float SIZE = 0.6;

uniform float uTime;

void generateLine(int index) {
    gl_Position = gl_in[index].gl_Position;
    EmitVertex();

    vec3 cur = vNormal[index];

    gl_Position = gl_in[index].gl_Position + vec4(cur, 0.0f) * SIZE;
    EmitVertex();
    EndPrimitive();
}

void main() {
    generateLine(0);
    generateLine(1);
    generateLine(2);
}
