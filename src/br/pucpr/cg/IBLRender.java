package br.pucpr.cg;

import br.pucpr.mage.*;
import br.pucpr.mage.IBL.IBLMaterial;
import br.pucpr.mage.IBL.SolidColor;
import br.pucpr.mage.Window;
import br.pucpr.mage.normals.NormalsMaterial;
import br.pucpr.mage.phong.DirectionalLight;
import br.pucpr.mage.phong.SkyMaterial;
import org.joml.Matrix4f;
import org.joml.Vector3f;


import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE_CUBE_MAP;

/**
 * Created by Zadooke on 23/10/2017.
 */
public class IBLRender implements Scene {

    private static final String PATH = "resources/";
    private Keyboard keys = Keyboard.getInstance();

    private GoodCamera camera = new GoodCamera();
    private float lookX = 0.0f;
    private float lookY = 0.0f;

    private DirectionalLight light;

    private Mesh mesh;
    private IBLMaterial material;

    private SolidColor solidColor;

    private float rotation = 0.0f;

    private Mesh lion;

    private Mesh skydome;
    private SkyMaterial skyMaterial;

    private NormalsMaterial normalsMaterial;

    @Override
    public void init() {
        glEnable(GL_DEPTH_TEST);
        //glEnable(GL_CULL_FACE);
        glEnable(GL_TEXTURE_CUBE_MAP);

        glPolygonMode(GL_FRONT_FACE, GL_LINE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        camera.getPosition().z = -2.5f;

        light = new DirectionalLight(
                new Vector3f( 1.0f, -1.0f, 1.0f),    //direction
                new Vector3f( 0.1f,  0.1f, 0.1f),    //ambient
                new Vector3f( 1.0f,  1.0f, 1.0f),    //diffuse
                new Vector3f( 1.0f,  1.0f, 1.0f));   //specular

        //Texture cube = new Texture(PATH + "cubeMap.jpg");

        mesh = MeshFactory.createSphereNM(20, 20);
        //mesh = MeshFactory.createSquareWithNormalMap();

        solidColor = new SolidColor();

        // Carrega o Material de Debug
        normalsMaterial = new NormalsMaterial();


        String fileName = "Barrel/barrel";

        lion = MeshFactory.CreateImportedMesh(MeshData.loadObj(fileName));
        Texture lionAlbedo = new Texture(PATH + fileName + "_D.jpg");
        Texture lionNormal = new Texture(PATH + fileName + "_N.jpg");
        Texture lionSpecular = new Texture(PATH + fileName + "_S.jpg");
        Texture lionAO = new Texture(PATH + fileName + "_AO.jpg");

        material = new IBLMaterial(
                new Vector3f(1.0f, 1.0f, 1.0f), //ambient
                new Vector3f(0.7f, 0.7f, 0.7f), //diffuse
                new Vector3f(0.5f, 0.5f, 0.5f), //specular
                128.0f);                         //specular power

        material.setTexture(lionAlbedo);
        material.setNormal(lionNormal);
        material.setSpecular(lionSpecular);
        material.setOcclusion(lionAO);
    }

    private boolean spin = true;
    private float reflectivity = 0.0f;

    @Override
    public void update(float secs) {
        float SPEED = 1;

        if (keys.isPressed(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GLFW_TRUE);
            return;
        }

        camera.update(keys, secs, SPEED);

        if (keys.isPressed(GLFW_KEY_P)) {
            spin = !spin;
        }

        if (keys.isPressed(GLFW_KEY_O)) {
            normalDebug = !normalDebug;
        }

        if (keys.isDown(GLFW_KEY_LEFT_SHIFT)) {
            SPEED *= 10;
        }

        if(keys.isDown(GLFW_KEY_C) && reflectivity > 0.0f){
            reflectivity -= secs;
            System.out.println(reflectivity);
        } else if(keys.isDown(GLFW_KEY_V) && reflectivity < 1.0f){
            reflectivity += secs;
            System.out.println(reflectivity);
        }

        if(spin){
            rotation += secs / 3;
            light.getDirection().x = -5;
            return;
        }
        else{
            if(keys.isDown(GLFW_KEY_N)){
                rotation += secs;
            } else if(keys.isDown(GLFW_KEY_M)){
                rotation -= secs;
            }
        }

        if (keys.isDown(GLFW_KEY_J)) {
            lookX -= SPEED * secs;
        } else if (keys.isDown(GLFW_KEY_K)) {
            lookX += SPEED * secs;
        }

        if (keys.isDown(GLFW_KEY_SPACE)) {
            lookX = 0;
            rotation = 0.0f;
        }

        light.getDirection().x = lookX;
    }

    private void drawMesh(Mesh mesh, Material mat) {
        Shader shader = mat.getShader();
        shader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .setUniform("uCameraPosition", camera.getPosition())
                .setUniform("uReflectivity", reflectivity);
        light.apply(shader);
        shader.unbind();

        mesh.setUniform("uWorld", new Matrix4f().rotateY(rotation));
        mesh.draw(mat);
    }

    private boolean normalDebug = false;
    public void drawNormals(Mesh mesh){
        if(normalDebug)
            return;;

        Shader shader = normalsMaterial.getShader();
        shader.bind()
                .setUniform("uProjection", camera.getProjectionMatrix())
                .setUniform("uView", camera.getViewMatrix())
                .setUniform("uCameraPosition", camera.getPosition());
        light.apply(shader);
        shader.unbind();

        mesh.draw(normalsMaterial);
    }

    @Override
    public void draw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        drawMesh(lion, material);
        drawNormals(lion);
    }

    @Override
    public void deinit() {

    }

    public static void main(String[] args) {
        new Window(new IBLRender(), "IBL Render", 1024, 748).show();
    }
}
