#version 400

uniform mat4 uProjection;
uniform mat4 uView;
uniform mat4 uWorld;

uniform vec3 uCameraPosition;

in vec3 aPosition;
in vec3 aNormal;
in vec2 aTexCoord;
in vec3 aTangent;

out vec3 vNormal;
out vec3 vViewPath;
out vec2 vTexCoord;

out mat3 vTBN;

void main() {
	vec4 worldPos = uWorld * vec4(aPosition, 1.0);  // Calcula a posição do mundo do obj
	gl_Position = uProjection * uView * worldPos;   // Posição

	vNormal = (uWorld * vec4(aNormal, 0.0)).xyz;    // Normal do vertice na posição do vertice
	vViewPath = uCameraPosition - worldPos.xyz;     // Caminho da camera para o vertice
	vTexCoord = aTexCoord;                          // Coordenada de textura


    // Normal Mapping
	vec3 bitangent = cross(aTangent, aNormal);
    vec3 t = normalize(vec3(uWorld * vec4(aTangent, 0.0)));
    vec3 b = normalize(vec3(uWorld * vec4(bitangent, 0.0)));
    vec3 n = normalize(vec3(uWorld * vec4(aNormal, 0.0)));

    vTBN = mat3(t, b, n);

}
