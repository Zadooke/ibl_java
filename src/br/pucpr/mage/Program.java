package br.pucpr.mage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Program {
    public static void main(String[] args) {
        File file = Paths.get(".", "resources", "Cube.obj").normalize().toFile();
        List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        MeshData meshData = ObjParser.GetMesh(lines);
    }
}
