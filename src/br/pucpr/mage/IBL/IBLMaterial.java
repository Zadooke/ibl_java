package br.pucpr.mage.IBL;

import br.pucpr.mage.SimpleMaterial;

import br.pucpr.mage.Material;
import br.pucpr.mage.Shader;
import br.pucpr.mage.Texture;
import org.joml.Vector3f;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

/**
 * Created by Zadooke on 23/10/2017.
 */
public class IBLMaterial extends SimpleMaterial {
    private static final String PATH = "/br/pucpr/mage/resource/IBL/";

    private Vector3f ambientColor;
    private Vector3f diffuseColor;
    private Vector3f specularColor;
    private float specularPower;
    private Map<String, Texture> textures = new HashMap<>();

    private Shader shader = Shader.loadProgram("/br/pucpr/mage/resource/IBL/ibl");

    public IBLMaterial(Vector3f ambientColor, Vector3f diffuseColor, Vector3f specularColor, float specularPower) {
        super(PATH + "ibl.vert", PATH + "ibl.frag");

        this.ambientColor = ambientColor;
        this.diffuseColor = diffuseColor;
        this.specularColor = specularColor;
        this.specularPower = specularPower;
    }

    public IBLMaterial setNormal(Texture normalMap) {
        return setTexture("uNormal", normalMap);
    }

    public Vector3f getAmbientColor() {
        return ambientColor;
    }
    public Vector3f getDiffuseColor() {
        return diffuseColor;
    }
    public Vector3f getSpecularColor() {
        return specularColor;
    }
    public float getSpecularPower() {
        return specularPower;
    }

    public void setSpecularPower(float specularPower) {
        this.specularPower = specularPower;
    }    
    
    public Material setTexture(Texture texture) {
        return setTexture("uMainTex", texture);
    }

    public IBLMaterial setTexture(String name, Texture texture) {
        if (texture == null) {
            textures.remove(name);
        } else {
            textures.put(name, texture);
        }
        return this;
    }

    @Override
    public void apply() {
        shader.setUniform("uAmbientMaterial", ambientColor);
        shader.setUniform("uDiffuseMaterial", diffuseColor);
        shader.setUniform("uSpecularMaterial", specularColor);
        shader.setUniform("uSpecularPower", specularPower);
        
        int texCount = 0;        
        for (Map.Entry<String, Texture> entry : textures.entrySet()) {
            glActiveTexture(GL_TEXTURE0 + texCount);
            entry.getValue().bind();
            shader.setUniform(entry.getKey(), texCount);
            texCount = texCount + 1;            
        }
    }

    @Override
    public Shader getShader() {
        return shader;
    }

    @Override
    public void setShader(Shader shader) {
        if (shader == null) {
            throw new IllegalArgumentException("Shader cannot be null!");
        }
        this.shader = shader;
    }

    public IBLMaterial setOcclusion(Texture occlusion) {
        return setTexture("uOcclusion", occlusion);
    }

    public IBLMaterial setSpecular(Texture specular) {
        return setTexture("uSpecular", specular);
    }
}
