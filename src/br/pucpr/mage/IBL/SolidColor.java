package br.pucpr.mage.IBL;

import br.pucpr.mage.SimpleMaterial;

/**
 * Created by Zadooke on 23/10/2017.
 */
public class SolidColor extends SimpleMaterial {
    private static final String PATH = "/br/pucpr/mage/resource/IBL/";

    public SolidColor() {
        super(PATH + "solid.vert", PATH + "solid.frag");
    }
}
