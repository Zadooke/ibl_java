package br.pucpr.mage;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class MeshData {
    public ArrayList<Vector3f> Vertices;
    public ArrayList<Vector3f> Normals;
    public ArrayList<Vector2f> UVs;
    public ArrayList<Integer> Triangles;

    public static MeshData loadObj(String name){
        File file = Paths.get(".", "resources", name + ".obj").normalize().toFile();
        List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ObjParser.GetMesh(lines);
    }

    public MeshData(ArrayList<Vector3f> vertices, ArrayList<Vector2f> uvs, ArrayList<Vector3f> normals, ArrayList<Integer> triangles) {
        Vertices = vertices;
        Normals = normals;
        UVs = uvs;
        Triangles = triangles;
    }
}